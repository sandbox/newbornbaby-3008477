<?php

/**
 * Ubercart Hotel Booking Upgrades & Addons.
 *
 * @file
 * Provides upgrade & addon functionality to ubercart hotel module.
 * @author Will Vincent (tcindie) <tcindie at gmail dot com>
 * @todo this file needs some love
 */


/**
 * Implementation of hook_menu().
 */
function hotel_addons_upgrades_menu() {
  $items = array();
  $items['admin/store/hotel_booking/upgrades_addons'] = array(
    'title'            => 'Upgrades & Addons',
    'page callback'    => 'hotel_addons_upgrades_show_items',
    'access arguments' => array('administer hotel booking settings'),
    'type'             => MENU_LOCAL_TASK,
    'weight'           => 3,
    'file'             => 'hotel_addons_upgrades_admin.inc',
  );
  $items['admin/store/hotel_booking/upgrades_addons/settings'] = array(
    'title'            => 'Settings',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('hotel_addons_upgrades_settings_form'),
    'access arguments' => array('administer hotel booking settings'),
    'type'             => MENU_LOCAL_TASK,
    'file'             => 'hotel_addons_upgrades_admin.inc',
  );
  $items['admin/store/hotel_booking/upgrades_addons/add'] = array(
    'title'            => 'Add Upgrade/Addon product',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('hotel_addons_upgrades_items_form'),
    'access arguments' => array('administer hotel booking settings'),
    'type'             => MENU_CALLBACK,
    'file'             => 'hotel_addons_upgrades_admin.inc',
  );
  $items['admin/store/hotel_booking/upgrades_addons/%'] = array(
    'title'            => 'View/Edit Upgrade/Addon product',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('hotel_addons_upgrades_items_form', 4),
    'access arguments' => array('administer hotel booking settings'),
    'type'             => MENU_CALLBACK,
    'file'             => 'hotel_addons_upgrades_admin.inc',
  );
  $items['admin/store/hotel_booking/upgrades_addons/%/delete'] = array(
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('hotel_addons_upgrades_items_delete_form', 4),
    'access arguments' => array('administer hotel booking settings'),
    'type'             => MENU_CALLBACK,
    'file'             => 'hotel_addons_upgrades_admin.inc',
  );
  $items['booking_upgrades/%'] = array(
    'page callback' => 'drupal_get_form',
    'access arguments' => array('view hotel search results'),
    'page arguments' =>  array('hotel_addons_upgrades_customer_form', 1),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

function hotel_addons_upgrades_customer_form($form, &$form_state, $cart_item_id = 0) {
  // If the cart item id isn't set, redirect to the cart.
  if ($cart_item_id == 0) {
    drupal_goto('cart');
  }
  // Load the data array from the cart item, if it's empty or of the wrong product type, redirect to the cart.
  $cart_data = db_query('SELECT data FROM {uc_cart_products} WHERE cart_item_id = :id', array(':id' => $cart_item_id))
    ->fetchField();
  if ($cart_data) {
    $cart_data = unserialize($cart_data);
  }
  if (!$cart_data || ($cart_data['module'] != "hotel_booking")) {
    drupal_goto('cart');
  }

  $form['header'] = array(
    '#value'  => variable_get('hotel_booking_addons_page_text', t('<p>The following upgrades and addons are available during your stay.</p>')),
  );
  $form['products'] = array(
    '#tree'   => TRUE,
    '#value'  => '',
  );
  $context = array(
    'revision' => 'themed',
    'type' => 'cart_item',
    'subject' => array(),
  );

  $roomtypeid = db_select('uc_cart_products', 'p')
    ->fields('p', array('nid'))
    ->condition('p.cart_item_id', $cart_item_id, '=')
    ->execute()
    ->fetchField();

  $results = db_query('SELECT * FROM {hotel_booking_upgrades} ORDER BY daystay, model');
  while ($result = $results->fetchAssoc()) {
    
    $this_nid = db_select('uc_products', 'p')
      ->fields('p', array('nid'))
      ->condition('p.model', $result['model'])
      ->execute()
      ->fetchField();
    /**
     * TODO:
     *   when product is updated/deleted, upgrades and addons model needs to be updated/deleted.
     * If a model is changed it will not get updated in hotel_booking_upgrades table
     * If() below is to to circumvent the issue.
     */
    if (!$this_nid) {
      continue;
    }
    $node = node_load($this_nid);

    if ($result['availableroomtypes']) {
      $roomtypesettings = unserialize($result['availableroomtypes']);
      $appliestothisroomtype = isset($roomtypesettings[$roomtypeid]) && $roomtypesettings[$roomtypeid];
    }
    else {
      $appliestothisroomtype = TRUE;
    }

    if ($appliestothisroomtype) { 
      $item_table_rows = hotel_addons_upgrades_item_table($result, $cart_data, $node);
    }
    else {
      $item_table_rows = array();
    } 
    $item_available = FALSE;
    $number_of_items = 0;

    for ($row = 0; $row < sizeof($item_table_rows); $row++) {

      if ($item_table_rows[$row]['qty']) {
        $item_available = TRUE;
      }

      else {
        $item_table_rows[$row]['qty'] = t('Not Available');
      }

    }
    $edit_item = isset($cart_data) && isset($cart_data['booking_upgrades']) && isset($cart_data['booking_upgrades'][$node->nid]);
    if ($item_available) {
      $teaser = node_view($node, 'teaser');
      $teaser = drupal_render($teaser);
      $price = array('price' => $node->sell_price, 'qty' => 1);
      $fields = array(
        '#type' => 'fieldset',
        '#collapsiblen' => TRUE,
        '#collapsed' => !$edit_item,
        '#title' => $node->title,
        /* '#description' => '<div class="addon-item-desc">' . $node->teaser . '</div>', */
        '#description' => '<div class="addon-item-desc">' . $teaser . '</div>',
        'data' => array(
          'readmore' => array(
            '#value' => l('read more','node/' . $node->nid, array('attributes' => array('target' => '_blank'))),
          ),
          'price' => array(
            '#prefix' => '<div class="addon-item-price">',
            '#suffix' => '</div>',
            '#type' => 'markup',
            /* '#value' => 'Base rate: ' . uc_price($price, $context), */
            '#value' => 'Base rate: ' . uc_currency_format($price['price']),
          ),
        ),
      );
      if ($result['daystay'] == 1) {
        $fields['data']['daystay'] = array('0'=>array(
          '#prefix' => '<div class="addon-item-multiday">',
          '#suffix' => '</div>',
          '#type' => 'markup',
          '#value' => t('This item can be added to individual days of your stay. Click on Available Dates to see availability and make your selection.'),
        ));
      }
      $attributes = array();
      $defaults = NULL;
      if (isset($node->attributes)) {
        if ($edit_item) {
          $data = db_query('SELECT data FROM {uc_cart_products} WHERE cart_item_id = ?', array($cart_data['booking_upgrades'][$node->nid]))
            ->fetchObject();
          if ($data) {
            $data = unserialize($data->data);
            if (isset($data['attributes'])) {
              $defaults = $data['attributes'];
            }
          }
        }
        foreach ($node->attributes as $aid => $attr) {
          // Generate a list of options.
          uasort($attr->options, 'element_sort');
          $options = array();
          foreach ($attr->options as $oid => $opt) {
            /* $options[$oid] = $opt->name . ($opt->price > 0 ? ' (+' . /\*uc_cart_currency() .*\/ ' ' . uc_price($opt->price, array('revision' => 'original')) . ')' : ''); */
            $options[$oid] = $opt->name . ($opt->price > 0 ? ' (+' . ' ' . uc_currency_format($opt->price) . ')' : '');
          }
          if (count($options) > 0) {
            // Get the default option or cart item option
            if (isset($defaults) && array_key_exists($aid, $defaults)) {
              $default = $defaults[$aid];
            }
            else {
              $default = $attr->default_option;
            }
            $attributes[$aid] = array(
              '#title' => $attr->label,
              '#type' => 'select',
              '#options' => $options,
              '#default_value' => $default,
            );
          }
        }
      }
      $fields['attributes'] = $attributes;
      $form['products'][$result['daystay']][$result['model']] = $fields;
      $form['products']['node'][$node->nid]['price'] = array(
        '#type'        => 'hidden',
        '#value'       => $node->sell_price,
      );
      $form['products']['node'][$node->nid]['title'] = array(
        '#type'        => 'hidden',
        '#value'       => $node->title,
      );
      if ($result['daystay'] == 0) {
        $header = array(t('Quantity'));
      }

      else {
        $header = array(t('Date'), t('Quantity'));
      }
      if (isset($item_table_rows['options'])) {
        $header[] = t('Options');
      }
      if($result['daystay'] == 1) {
        $form['products'][$result['daystay']][$result['model']]['more'] = array(
          '#type'        => 'fieldset',
          '#title'       => t('Available Dates'),
          '#collapsible' => TRUE,
          '#collapsed'   => FALSE,
        );
        $form['products'][$result['daystay']][$result['model']]['more']['table'] = array(
          '#type' => 'markup',
          '#markup' => '<div class="booking_addon_item_list">'. theme('table', array('header' => $header, 'rows' => $item_table_rows)) .'</div>',
        );
      }
      else {
        $form['products'][$result['daystay']][$result['model']]['table'] = array(
          '#type' => 'markup',
          '#markup' => '<div class="booking_addon_item_list">'. theme('table', array('header' => $header, 'rows' => $item_table_rows)) .'</div>',
        );
      }
      $number_of_items++;
    }
  }
  $form['products']['cart_item_id'] = array(
    '#type'        => 'hidden',
    '#value'       => $cart_item_id,
  );

  // Invoke hook_hotel_addons_upgrades_items().
  foreach (module_implements('hotel_addons_upgrades_items') as $module) {
    $func = $module .'_hotel_addons_upgrades_items';
    $func($form, $cart_item_id, $cart_data, $number_of_items);
  }
  if (isset($cart_data['booking_upgrades']) && is_array($cart_data['booking_upgrades'])) {
    $form['products']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Update Quantities'),
    );
    $form['products']['decline'] = array(
      '#type' => 'submit',
      '#value' => t('No Changes'),
    );
  }
  else {
    $form['products']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Add Selections'),
    );
    $form['products']['decline'] = array(
      '#type' => 'submit',
      '#value' => t('No Thanks'),
    );
  }
  if ($number_of_items) {
    drupal_set_title(variable_get('hotel_booking_addons_page_title', t('Upgrades & Addons')));

    // Ensure they're able to get back to edit these, even if they don't hit the "no thanks" button.
    if(!isset($cart_data['booking_upgrades'])) {
      $cart_data['booking_upgrades'] = array();
      $cart_data['booking_upgrades_order'] = array();
      db_update('uc_cart_products')
        ->fields(array('data' => serialize($cart_data)))
        ->condition('cart_item_id', $cart_item_id)
        ->execute();
    }
    return $form;
  }
  else {
    drupal_goto('cart');
  }
}


function hotel_addons_upgrades_customer_form_submit($form, &$form_state) {
  $cart = db_query('SELECT cart_item_id, cart_id, data FROM {uc_cart_products} WHERE cart_item_id = ?', array($form_state['values']['products']['cart_item_id']))
    ->fetchAssoc();
  $cart_data = unserialize($cart['data']);
  if ($form_state['clicked_button']['#value'] == t('No Thanks') || $form_state['clicked_button']['#value'] == t('No Changes')) {
    if(!isset($cart_data['booking_upgrades'])) {
      $cart_data['booking_upgrades'] = array();
      $cart_data['booking_upgrades_order'] = array();
      db_update('uc_cart_products')
        ->fields(array('data' => serialize($cart_data)))
        ->condition('cart_item_id', $form_state['values']['products']['cart_item_id'], '=')
        ->execute();
    }
    drupal_goto('cart');
  }
  $child_items = array();
  $values = $form_state['values'];
  foreach ($form_state['input']['qtys'] as $nid => $value) {
    $data = array();
    $data['module'] = 'hotel_addons_upgrades';
    $node = node_load($nid);
    $model = $node->model;
    $daystay = is_array($value) ? 1 : 0;
    if (isset($form_state['values']['products'][$daystay][$model])) {
      $data['attributes'] = array();
      $options = $form_state['values']['products'][$daystay][$model];
      if (isset($options['attributes'])) {
        foreach ($options['attributes'] as $aid => $oid) {
          $data['attributes'][$aid] = $oid;
        }
      }
    }
    // For single day stay. either at checkin or checkout.
    if ($daystay == 0) {
      $qty = $value;
      $hash = md5(mt_rand(0,32) . time());
      $data['child_id'] = $hash;
      $data['parent_id'] = $cart['cart_item_id'];
      // If this item was already in the cart, update it, otherwise add it.
      $result = !empty($cart_data['booking_upgrades'][$nid]) ? hotel_addons_upgrades_add_item($cart['cart_id'], $nid, $qty, $data, $cart_data['booking_upgrades'][$nid]) : hotel_addons_upgrades_add_item($cart['cart_id'], $nid, $qty, $data);
      if ($result) {
        $child_items[$nid] = $result;
        $child_items_order[$nid] = $hash;
      }
    }
    // For upgrades available for daily.
    else {
      $qtys = 0;
      $hash = md5(mt_rand(0,32) . time());
      $data['child_id'] = $hash;
      $data['parent_id'] = $cart['cart_item_id'];
      $data['days'] = array();
      foreach($value as $day => $qty) {
        $qtys += $qty;
        $data['days'][$day] = $qty;
      }
      // If this item was already in the cart, update it, otherwise add it.
      $result = !empty($cart_data['booking_upgrades'][$nid]) ? hotel_addons_upgrades_add_item($cart['cart_id'], $nid, $qtys, $data, $cart_data['booking_upgrades'][$nid]) : hotel_addons_upgrades_add_item($cart['cart_id'], $nid, $qtys, $data);
      if ($result) {
        $child_items[$nid] = $result;
        $child_items_order[$nid] = $hash;
      }
    }
  }
  $cart_data['booking_upgrades'] = $child_items;
  $cart_data['booking_upgrades_order'] = $child_items_order;
  db_update('uc_cart_products')
    ->fields(array('data' => serialize($cart_data)))
    ->condition('cart_item_id', $form_state['values']['products']['cart_item_id'], '=')
    ->execute();
  drupal_set_message(t('Room Upgrades & Addons have been added to your cart.'));

  drupal_goto('cart');
}


function hotel_addons_upgrades_add_item($cart_id = 0, $nid = 0, $qty = 0, $data = array(), $prior_item_id = FALSE) {
  // If we've gotten here some how without a valid node id or cart id, get out.
  if (!$nid || !$cart_id) {
    return FALSE;
  }

  // If item exists, and still has a quantity > 0, update it.
  if ($qty && $prior_item_id) {
    db_update('uc_cart_products')
      ->fields(array(
          'qty'=> $qty,
          'changed' => time(),
          'data' => serialize($data),
        ))
      ->condition('cart_item_id', $prior_item_id, '=')
      ->execute();
    $insert_id = $prior_item_id;
  }

  // If item exists, but quantity has been changed to 0, delete it.
  elseif (!$qty && $prior_item_id) {
    db_delete('uc_cart_products')
      ->condition('cart_item_id', $prior_item_id)
      ->execute();
    return FALSE;
  }

  // If item didn't exist before, add it.
  elseif ($qty && !$prior_item_id) {
    $insert_id = db_insert('uc_cart_products')
      ->fields(array(
          'cart_id' => $cart_id,
          'nid' => $nid,
          'qty' => $qty,
          'changed' => time(),
          'data' => serialize($data),
        ))
      ->execute();
  }

  // If none of the above scenarios apply for some reason, give up and get out.
  else {
    return FALSE;
  }

  // Assuming everything went as planned, we're returning a valid cart_item_id.
  return $insert_id;
}


function hotel_addons_upgrades_item_table(&$item_settings = array(), &$cart_data = array(), &$node) {
  // Exit this function immediately if any of the required data isn't set.
  if (!$node || !$cart_data || !$item_settings) {
    return;
  }
  $days = array();
  $daily = FALSE;
  if (isset($cart_data['booking_upgrades']) && isset($cart_data['booking_upgrades'][$node->nid])) {
    $item = db_query('SELECT * FROM {uc_cart_products} WHERE cart_item_id = ?', array($cart_data['booking_upgrades'][$node->nid]))
      ->fetchObject();
    $item_data = unserialize($item->data);
    if (!empty($item_data['days'])) {
      $days = $item_data['days'];
      $daily = TRUE;
    }
  }
  else {
    $item = new stdClass();
    $item->qty = 0;
  }
  $daysofweek = unserialize($item_settings['daysofweek']);
  $months = unserialize($item_settings['monthsofyear']);

  for ($night = 0; $night < count($cart_data['nights']); $night++) {
    $thisdate = strtotime($cart_data['nights'][$night]);
    $formatted_date = date('l F j, Y', $thisdate);
    $dayofweek = date('w', $thisdate);
    /**
     * For checkin day
     */
    if ($night == 0 && $item_settings['checkin']) {
      $formatted_date = t('Check In: ') . $formatted_date;

      if (_available_this_day($dayofweek, $daysofweek) && _available_this_month(date('n', $thisdate), $months)) {
        $qty = '<select name="qtys['. $node->nid .']['. $cart_data['nights'][$night] .']">';
        if ($daily) {
          $qty .= '<option';
          if ($days[$cart_data['nights'][$night]] == 0) {
            $qty .= ' selected="selected">0</option><option';
          }
          else {
            $qty .= '>0</option><option';
          }
          if ($days[$cart_data['nights'][$night]] == 1) {
            $qty .= ' selected="selected">1</option>';
          }
          else {
            $qty .= '>1</option>';
          }

          if (($cart_data['adults'] + $cart_data['children']) > 1 && $item_settings['number']) {
            for ($i = 2; $i <= ($cart_data['adults'] + $cart_data['children']); $i++) {
              $qty .= '<option';
              if ($days[$cart_data['nights'][$night]] == $i) {
                $qty .= ' selected="selected">'. $i .'</option>';
              }
              else {
                $qty .= '>'. $i .'</option>';
              }
            }

          }

        }
        else {
          $qty .= '<option';
          if ($item->qty == 0) {
            $qty .= ' selected="selected">0</option><option';
          }
          else {
            $qty .= '>0</option><option';
          }
          if ($item->qty == 1) {
            $qty .= ' selected="selected">1</option>';
          }
          else {
            $qty .= '>1</option>';
          }

          if (($cart_data['adults'] + $cart_data['children']) > 1 && $item_settings['number']) {
            for ($i = 2; $i <= ($cart_data['adults'] + $cart_data['children']); $i++) {
              $qty .= '<option';
              if ($item->qty == $i) {
                $qty .= ' selected="selected">'. $i .'</option>';
              }
              else {
                $qty .= '>'. $i .'</option>';
              }
            }

          }

        }
        $qty .= '</select>';

      }

      else {
        $qty = NULL;
      }

    }

    elseif ($night == 0 && !$item_settings['checkin']) {
      $formatted_date = t('Check In: ') . $formatted_date;
      $qty = NULL;
    }

    elseif ($night > 0) {
      if (_available_this_day($dayofweek, $daysofweek) && _available_this_month(date('n', $thisdate), $months)) {
        $qty = '<select name="qtys['. $node->nid .']['. $cart_data['nights'][$night] .']">';
        if ($daily) {
          $qty .= '<option';
          if ($days[$cart_data['nights'][$night]] == 0) {
            $qty .= ' selected="selected">0</option><option';
          }
          else {
            $qty .= '>0</option><option';
          }
          if ($days[$cart_data['nights'][$night]] == 1) {
            $qty .= ' selected="selected">1</option>';
          }
          else {
            $qty .= '>1</option>';
          }

          if (($cart_data['adults'] + $cart_data['children']) > 1 && $item_settings['number']) {
            for ($i = 2; $i <= ($cart_data['adults'] + $cart_data['children']); $i++) {
              $qty .= '<option';
              if ($days[$cart_data['nights'][$night]] == $i) {
                $qty .= ' selected="selected">'. $i .'</option>';
              }
              else {
                $qty .= '>'. $i .'</option>';
              }
            }

          }

        }
        else {
          $qty .= '<option';
          if ($item->qty == 0) {
            $qty .= ' selected="selected">0</option><option';
          }
          else {
            $qty .= '>0</option><option';
          }
          if ($item->qty == 1) {
            $qty .= ' selected="selected">1</option>';
          }
          else {
            $qty .= '>1</option>';
          }

          if (($cart_data['adults'] + $cart_data['children']) > 1 && $item_settings['number']) {
            for ($i = 2; $i <= ($cart_data['adults'] + $cart_data['children']); $i++) {
              $qty .= '<option';
              if ($item->qty == $i) {
                $qty .= ' selected="selected">'. $i .'</option>';
              }
              else {
                $qty .= '>'. $i .'</option>';
              }
            }

          }

        }
        $qty .= '</select>';
      }
      else {
        $qty = NULL;
      }
    }
    $rows[] = array('date' => $formatted_date, 'qty' => $qty);
  }
  /* set checkout date */
  $thisdate = strtotime('+1 day', $thisdate);
  if ($item_settings['checkout']) {
    $dayofweek = date('w', $thisdate);
    $thisday = date('Y-m-d', $thisdate);
    if (_available_this_day($dayofweek, $daysofweek) && _available_this_month(date('n', $thisdate), $months)) {
      $qty = '<select name="qtys['. $node->nid .']['. date('Y-m-d H:i:s', $thisdate) .']">';
      if ($daily) {
        $selected = FALSE;
        if (empty($days[$thisday])) {
          $qty .= '<option selected="selected">0</option><option>1</option>';
          $selected = TRUE;
        }
        elseif ($days[$thisday] == 1) {
          $qty .= '<option>0</option><option selected="selected">1</option>';
          $selected = TRUE;
        }
        else {
          $qty .= '<option>0</option><option>1</option>';
        }
        if (($cart_data['adults'] + $cart_data['children']) > 1 && $item_settings['number']) {
          for ($i = 2; $i <= ($cart_data['adults'] + $cart_data['children']); $i++) {
            $qty .= '<option';
            if (!$selected && $days[$thisday] == $i) {
              $qty .= ' selected="selected">'. $i .'</option>';
            }
            else {
              $qty .= '>'. $i .'</option>';
            }
          }

        }

      }
      else {
        $qty .= '<option';
        if ($item->qty == 0) {
          $qty .= ' selected="selected">0</option><option';
        }
        else {
          $qty .= '>0</option><option';
        }
        if ($item->qty == 1) {
          $qty .= ' selected="selected">1</option>';
        }
        else {
          $qty .= '>1</option>';
        }

        if (($cart_data['adults'] + $cart_data['children']) > 1 && $item_settings['number']) {
          for ($i = 2; $i <= ($cart_data['adults'] + $cart_data['children']); $i++) {
            $qty .= '<option';
            if ($item->qty == $i) {
              $qty .= ' selected="selected">'. $i .'</option>';
            }
            else {
              $qty .= '>'. $i .'</option>';
            }
          }

        }

      }
      $qty .= '</select>';
    }
    else {
      $qty = NULL;
    }
  }
  else {
    $qty = NULL;
  }
  $formatted_date = t('Check Out: ') . date('l F j, Y', $thisdate);
  $rows[] = array('date' => $formatted_date, 'qty' => $qty);

  if ($item_settings['daystay'] > 0) {
    return $rows;
  }
  else {
    // Figure whether stay based item is available, and if so return qty.
    for ($z = 0; $z < count($rows); $z++) {
      if($rows[$z]['qty']) {
        // remove the date portion of this item's select tag name.
        $qty = '<select name="qtys['. $node->nid . substr($rows[$z]['qty'], (strlen($node->nid) + 40));
        $item_row = array(array('qty' => $qty));
      }
    }
    return isset($item_row) ? $item_row : array(array('qty' => NULL));
  }
}


function _available_this_day($dayofweek, $days = array()) {
  switch ($dayofweek) {
    case 0:
      return ($days['Sun']) ? TRUE : FALSE;
    case 1:
      return ($days['Mon']) ? TRUE : FALSE;
    case 2:
      return ($days['Tue']) ? TRUE : FALSE;
    case 3:
      return ($days['Wed']) ? TRUE : FALSE;
    case 4:
      return ($days['Thr']) ? TRUE : FALSE;
    case 5:
      return ($days['Fri']) ? TRUE : FALSE;
    case 6:
      return ($days['Sat']) ? TRUE : FALSE;
  }
  return FALSE;
}

function _available_this_month($month, $months = array()) {
  return ($months[$month]) ? TRUE : FALSE;
}


/**
 * Implementation of hook_cart_display().
 */
function hotel_addons_upgrades_uc_cart_display($item) {
    $optname = array();
    $node = node_load($item->nid);
    $element = array();
    $element['nid'] = array('#type' => 'value', '#value' => $node->nid);
    $element['module'] = array('#type' => 'value', '#value' => 'hotel_addons_upgrades');
    $room = db_query('SELECT n.title FROM {node} AS n, {uc_cart_products} AS c WHERE c.cart_item_id = ? AND c.nid = n.nid', array($item->data['parent_id']))
      ->fetchObject();
    if (array_key_exists('attributes', $item->data) && is_array($item->data['attributes']) && count($item->data['attributes']) > 0) {
      foreach ($node->attributes as $aid =>$attr) {
        if (array_key_exists($aid, $item->data['attributes'])) {
          foreach ($attr->options as $oid => $opt) {
            if ($oid == $item->data['attributes'][$aid]) {
              $optname[] =  $opt->name;
              break;
            }
          }
        }
      }
    }
    if (count($optname) > 0) {
      $optname = '<ul class="product-description"><li>' . implode('</li><li>', $optname) . '</li></ul>';
    }
    else {
      $optname = '';
    }
    $element['title'] = array(
      '#type' => 'markup',
      '#markup' => l($room->title .': '. $item->title, 'booking_upgrades/'. $item->data['parent_id']) . $optname,
    );
    $context = array(
      'revision' => 'altered',
      'location' => 'cart-item',
      'subject' => array(
        'cart_item' => $item,
      ),
      'extras' => array(
        'node' => $node,
      ),
    );
    $price_info = array(
      'price' => $item->price,
      'qty' => $item->qty,
    );

    $element['#total'] = $item->price * $item->qty;
    $element['data'] = array('#type' => 'hidden', '#value' => serialize($item->data));
    $element['qty'] = array(
      '#type'          => 'value',
      '#value'         => $item->qty,
      '#default_value' => $item->qty,
      'display' => array(
        '#type' => 'markup',
        '#markup' => $item->qty,
      ),
    );
    if (variable_get('hotel_booking_teaser_in_cart', FALSE)) {
      $description = '<div>'. $node->teaser .'</div>';
    }
    else {
      $description = '';
    }
    $element['description'] = array('#markup' => $description);
    return $element;
}
